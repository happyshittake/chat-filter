const autocannon = require('autocannon')
const bootstrap = require('../bootstrap')

let buf = Buffer.from(JSON.stringify({
  user_id: 99,
  text: 'Ach it was hopeless. That was what ye felt. These bastards. What can ye do but. Except start again so he started again. That was what he did he started again … ye just plough on, ye plough on, ye just fucking plough on … ye just fucking push ahead, ye get fucking on with it.'
}))

const instance = autocannon({
  url: 'http://localhost:3000/chat',
  method: 'POST',
  body: buf,
  headers: {
    'Content-Type': 'application/json'
  },
  connections: 100
}, console.log)

autocannon.track(instance)

process.once('SIGINT', () => {
  instance.stop()
})