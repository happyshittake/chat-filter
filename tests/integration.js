const tap = require('tap')
const fs = require('fs')
const FormData = require('form-data')
const buildFastify = require('../bootstrap')

tap.test('POST `/profane` route', t => {
  let fastify = buildFastify()

  t.teardown(() => fastify.close())

  let f = fs.createReadStream(fastify.appRoot + '/data/bad-word-list.csv')
  const form = FormData()
  form.append('file', f)

  fastify.inject({
    method: 'POST',
    url: '/profane',
    headers: form.getHeaders(),
    body: form
  }, (err, response) => {
    t.strictEqual(response.statusCode, 200, 'should return 200')
    t.strictEqual(response.payload, 'true', 'should return true')

    t.end()
  })
})

tap.test('POST `/chat/` route', t => {
  const fastify = buildFastify()

  t.teardown(() => fastify.close())

  fastify.inject({
    method: 'POST',
    url: '/chat',
    body: {
      user_id: 1,
      text: 'ash0le'
    }
  }, (err, response) => {
    t.strictEqual(response.statusCode, 200, 'http code should be 200')
    t.strictEqual(response.payload, 'false', 'should return false')
    fastify.redis.get(1).then((res) => {
      t.strictEqual(res > 0, true, 'should be greater than 0')
    })
  })

  fastify.inject({
    method: 'POST',
    url: '/chat',
    body: {
      user_id: 2,
      text: 'blablabla'
    }
  }, (err, response) => {
    t.strictEqual(response.statusCode, 200, 'http code 200')
    t.strictEqual(response.payload, 'true', 'should return true')
    fastify.redis.get(2).then((res) => {
      t.strictEqual(res, null, 'has value of null')
      t.end()
    })
  })
})


