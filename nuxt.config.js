module.exports = {
  css: [
    // Load a Node.js module directly (here it's a Sass file)
    {src: 'bulma/bulma.sass', lang: 'sass'}
  ],
  build: {
    postcss: {
      plugins: {
        'postcss-custom-properties': false
      }
    }
  }
}