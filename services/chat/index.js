const fp = require('fastify-plugin')

module.exports = fp(function (fastify, opts, next) {
  fastify.register(require('./post-chat'), opts)

  next()
})