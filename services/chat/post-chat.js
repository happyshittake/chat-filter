const httpErrors = require('http-errors')

const opts = {
  schema: {
    body: {
      type: 'object',
      properties: {
        user_id: {type: 'number'},
        text: {type: 'string'}
      }
    }
  }
}

module.exports = async function routes (fastify, options) {
  fastify.post('/', opts, async (request, reply) => {
    const {filter, redis, queue} = fastify
    let isProfane = false

    if (filter.isProfane(request.body.text)) {
      try {
        await redis.incr(request.body.user_id)
      } catch (e) {
        return httpErrors(500, e)
      }

      isProfane = true
      return false
    }

    //publish data to chat queue
    queue.create('chat', {
      user_id: request.body.user_id,
      text: request.body.text,
      is_profane: isProfane
    }).removeOnComplete(true).save() //on success write to db we want to delete the job from redis to free up memory
    return true
  })
}