const fp = require('fastify-plugin')
const fs = require('fs')
const Filter = require('./filter')

module.exports = fp(function (fastify, opts, next) {
  const arr = fs.readFileSync(fastify.appRoot + '/data/bad-word-list.csv').toString().split('\r\n')

  fastify.decorate('filter', new Filter({list: arr}))

  fastify.register(require('./post-update-list'), opts)

  next()
})