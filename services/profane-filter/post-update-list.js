module.exports = async function postUpdateList (fastify, opts) {
  fastify.post('/', async (request, reply) => {
    const file = request.raw.files.file

    await file.mv(fastify.appRoot + '/data/bad-word-list.csv')

    return true
  })
}