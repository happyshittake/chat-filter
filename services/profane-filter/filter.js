const levenshtein = require('fast-levenshtein')

class Filter {
  constructor (opts) {
    this.arr = opts.list
    this.regex = /[^a-zA-Z0-9|\$|\@]|\^/g
    this.tolerance = opts.tolerance || 0.1
    this.threshold = opts.threshold || 4
  }

  isProfane (str) {
    const tokens = str.split(' ').map(w => w.toLowerCase())

    for (let t of tokens) {
      let tolerance = 0
      if (t.length <= this.threshold) {
        if (this.arr.indexOf(t) > -1) {
          return true
        }
      } else {
        tolerance = Math.round(t.length * this.tolerance)
        if (this.arr.findIndex(e => levenshtein.get(t, e, {useCollator: true}) <= tolerance) > -1) {
          return true
        }
      }
    }

    return false
  }
}

module.exports = Filter