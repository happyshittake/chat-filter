#chat filter server
 to filter bad word from  chat server,
 using levenhstein algorithm to filter chat 
 from list of bad words.
 
 
 ### to run the app
 
 - make sure you've ran redis at localhost:6379
 - run the app
 ```bash
 npm start
 ```
 
 ### to run the worker
 
 - make sure you've ran redis at localhost:6379
 - run postgresql
 - run the worker
 ```bash
 npm run worker
 ```
 
 ### to run the test
 the test is just simple integration test to test all the available routes
 
 - run redis like to run the app
 - run the test
 ```bash
npm run test
``` 

### to run the benchmark
- run the app
- run the benchmark
```bash
npm run benchmark
```