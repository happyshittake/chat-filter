require('make-promises-safe')
const knex = require('knex')({
  client: 'pg',
  connection: {
    host: 'localhost',
    user: 'ndjoe',
    password: 'sempak',
    database: 'chat-filter'
  },
  debug: true
})
const queue = require('kue').createQueue()

function insertData (data) {
  return knex.insert(data).into('data')
}

// process data in chat queue
queue.process('chat', async (job, done) => {
  try {
    await insertData(job.data)
    done()
  } catch (e) {
    done(e)
  }
})



