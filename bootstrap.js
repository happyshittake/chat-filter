const Fastify = require('fastify')
const path = require('path')
const kue = require('kue')

module.exports = function buildFastify (opts = {logger: false}) {
  const fastify = Fastify({
    logger: opts.logger
  })

  fastify.decorate('appRoot', path.resolve(__dirname))
  fastify.decorate('queue', kue.createQueue())
  fastify.register(require('fastify-redis'), {host: 'localhost'})
  fastify.register(require('fastify-file-upload'), {
    safeFileNames: true
  })
  fastify.register(require('./services/profane-filter'), {
    prefix: '/profane'
  })
  fastify.register(require('./services/chat'), {
    prefix: '/chat'
  })
  fastify.register(require('fastify-cors'))
  // fastify.register(require('./plugins/nuxt-renderer'), require('./nuxt.config'))

  return fastify
}