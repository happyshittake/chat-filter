require('make-promises-safe')
const buildFastify = require('./bootstrap')
const cluster = require('cluster')
const numCPUS = require('os').cpus().length

const fastify = buildFastify({logger: true})

fastify.listen(3000, (err, addr) => {
  if (err) {
    throw err
  }
})

// for performance to use all cpu cores
// improve up to 10x performance in my macbook pro

// if (cluster.isMaster) {
//   console.log(`Master ${process.pid} is running`)
//
//   // Fork workers.
//   for (let i = 0; i < numCPUS; i++) {
//     cluster.fork()
//   }
//
//   cluster.on('exit', (worker, code, signal) => {
//     console.log(`worker ${worker.process.pid} died`)
//   })
// } else {
//   fastify.ready()
//   fastify.listen(3000, (err, addr) => {
//     if (err) {
//       throw err
//     }
//   })
// }